# Changelog
Project: WoWPerations Plugin Boilerplate
Description: This is a boilerplate plugin used [WoWPerations](https://www.wowperations.com.br). Built on top of WordPressPlugin Boilerplate.
Version: 0.1.1

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/)

## [0.1.1] - 2018-07-18

Initial plugin version

### Fixed
- Plugin Name

## [0.1.0] - 2018-07-18

Initial plugin version

### Added
- CHANGELOG.md
- README.md
- initial plugin architecture (based on WordPress Plugin Boilerplate)