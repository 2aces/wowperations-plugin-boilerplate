<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/wowperations/wowperations-plugin-boilerplate/
 * @since      0.1.0
 *
 * @package    Wowperations_Plugin_Boilerplate
 * @subpackage Wowperations_Plugin_Boilerplate/admin/partials
 */

?>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
