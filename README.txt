=== WoWPerations Plugin Boilerplate ===
Contributors: celsobessa
Donate link: https://bitbucket.org/2aces/wowperations-plugin-boilerplate/
Tags: comments, spam
Requires at least: 4.9.0
Tested up to: 4.9.7
Requires PHP: 7.2
Stable tag: 0.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This is a boilerplate plugin used [WoWPerations](https://www.wowperations.com.br). Built on top of WordPressPlugin Boilerplate.

== Description ==

This is a boilerplate plugin used [WoWPerations](https://www.wowperations.com.br). Built on top of WordPressPlugin Boilerplate and it follows WordPress Coding Standards and Object Oriented Programming as much as possible.

== Installation ==


1. Upload `wowperations-plugin-boilerplate` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

Please, check the [CHANGELOG.md] file for the full changelog. Below you will find the last 3 releases or updates.

= [0.1.1] - 2018-07-18 =

Initial plugin version

### Fixed
- Plugin Name

= [0.1.0] - 2018-07-18 =

Initial plugin version

### Added
- CHANGELOG.md
- README.md
- initial plugin architecture (based on WordPress Plugin Boilerplate)