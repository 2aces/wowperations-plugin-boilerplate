<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/wowperations/wowperations-plugin-boilerplate/
 * @since             0.1.0
 * @package           Wowperations_Plugin_Boilerplate
 *
 * @wordpress-plugin
 * Plugin Name:       WoWPerations Plugin Boilerplate
 * Plugin URI:        https://bitbucket.org/wowperations/wowperations-plugin-boilerplate/
 * Description:       This is a boilerplate plugin used by WoWPerations. Built on top of WordPressPlugin Boilerplate.
 * Version:           0.1.0
 * Author:            Celso Bessa, WoWPerations
 * Author URI:        https://www.wowperations.com.br
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wowperations-plugin-boilerplate
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'WOWPERATIONS_PLUGIN_BOILERPLATE_VERSION', '0.1.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented inwowperations_platform_woocommerce includes/class-wowperations-plugin-boilerplate-activator.php
 */
function activate_wowperations_plugin_boilerplate() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wowperations-plugin-boilerplate-activator.php';
	Wowperations_Platform_Woocommerce_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wowperations-plugin-boilerplate-deactivator.php
 */
function deactivate_wowperations_plugin_boilerplate() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wowperations-plugin-boilerplate-deactivator.php';
	Wowperations_Platform_Woocommerce_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wowperations_plugin_boilerplate' );
register_deactivation_hook( __FILE__, 'deactivate_wowperations_plugin_boilerplate' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wowperations-plugin-boilerplate.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.1.0
 */
function run_wowperations_plugin_boilerplate() {

	$plugin = new Wowperations_Plugin_Boilerplate();
	$plugin->run();

}
run_wowperations_plugin_boilerplate();
