<?php
/**
 * Fired during plugin activation
 *
 * @link       https://bitbucket.org/wowperations/wowperations-plugin-boilerplate/
 * @since      0.1.0
 *
 * @package    Wowperations_Plugin_Boilerplate
 * @subpackage Wowperations_Plugin_Boilerplate/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1.0
 * @package    Wowperations_Plugin_Boilerplate
 * @subpackage Wowperations_Plugin_Boilerplate/includes
 * @author     Celso Bessa <celso.bessa@wowperations.com.br>
 */
class Wowperations_Platform_Woocommerce_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function activate() {
		/**
		 * If WooCommerce is not present, don't activate plugin
		 */

		if ( ! class_exists( 'WooCommerce' ) ) {
			deactivate_plugins( 'wowperations-plugin-boilerplate.php' );
			wp_die( esc_html__( 'WoWPerations Plugin Boilerplate works only when WooCommerce is installed. Please install and activate WooCommerce.', 'wowperations-plugin-boilerplate' ), esc_html__( 'WoWPerations Plugin Boilerplate dependency check', 'wowperations-plugin-boilerplate' ), array(
				'back_link' => true,
			));
		}
	}

}
