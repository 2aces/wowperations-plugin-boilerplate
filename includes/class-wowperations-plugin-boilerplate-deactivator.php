<?php
/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/wowperations/wowperations-plugin-boilerplate/
 * @since      0.1.0
 *
 * @package    Wowperations_Plugin_Boilerplate
 * @subpackage Wowperations_Plugin_Boilerplate/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.1.0
 * @package    Wowperations_Plugin_Boilerplate
 * @subpackage Wowperations_Plugin_Boilerplate/includes
 * @author     Celso Bessa <celso.bessa@wowperations.com.br>
 */
class Wowperations_Platform_Woocommerce_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function deactivate() {

	}

}
